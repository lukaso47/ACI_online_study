module.exports = function(grunt) {
    require('load-grunt-config')(grunt, {
        configPath: 'grunt/config',
        jitGrunt: {
            customTasksDir: 'grunt/tasks',
            pluginsRoot: 'node_modules',
            staticMappings: {
                'webpack-dev-server':  'node_modules/grunt-webpack/tasks/webpack-dev-server.js'
            }
        },
    });
};