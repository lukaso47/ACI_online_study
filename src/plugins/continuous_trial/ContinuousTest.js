/*
This is where the real stuff happens... Again, we first import the libraries we need
 */
import 'jspsych';
import React from 'react';
import ReactDom from 'react-dom';
import { Fixcross } from '../../lib/react_components/fixcross';




/*
This is the library that can create, modify and play sounds...
 */
import {Noise, Sine} from "@thht/sounds";



/*
This is the actual plugin. It needs at least two things:
1. a so called getter with the name "info". (a getter is a method of a class that looks like a property from the outside)
2. a method called "trial" which executes the actual stuff

Also refer to here for general infos on how to create a jspsych plugin: https://www.jspsych.org/plugins/creating-a-plugin/
 */
class ContinuousTest {
    // This is the constructor. It gets called when the "new" function is called in the index.js file.
    // We basically prepare some variables here that do not depend on the configuration of this plugin...
    constructor() {
        this.trialinfo = null;
        this.display_element = null;
        this.sound = null;
    }

    // Sets the name of this plugin and defines what parameters can be used
    get info() {
        return {
            name: 'continuous_test',
            parameters: {
                n_sounds: {
                    type: jsPsych.plugins.parameterType.INT,
                    default: undefined,
                },
                snd_length: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: 0.3,
                },
                freq: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: undefined,
                },
                volume_db: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: undefined,
                },
                add_db: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: 0,
                },
                volume_db_noise: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: undefined,
                },
                noise_length: {
                    type: jsPsych.plugins.parameterType.FLOAT,
                    default: 0.1,
                },
            }
        }
    }

    /*
     This is the method that gets called by jspsych when this plugin is executed
     display_element is the html element in the browser that we are supposed to paint our content to.
     trial contains all the infos like parameters defined above etc...
     */
    trial(display_element, trial) {
        // First we store these parameters in the class itself. This way, we can access the information from other methods as well
        this.trialinfo = trial;
        this.display_element = display_element;

        // Now we generate our sine wave and noise...

        let current_delay;
        const white_noise = new Noise(this.trialinfo.noise_length);
        white_noise.db    = this.trialinfo.volume_db_noise;
        white_noise.amplify_db(this.trialinfo.add_db);
        white_noise.apply_sin_ramp(0.005);
        

        const sine_wave = new Sine(this.trialinfo.freq, this.trialinfo.snd_length - 2.01);
        sine_wave.apply_sin_ramp(0.005); // to avoid clicking
        sine_wave.db = this.trialinfo.volume_db;

        const start_sine = new Sine(this.trialinfo.freq, 0.3);
        start_sine.apply_sin_ramp(0.005); // to avoid clicking
        start_sine.db = this.trialinfo.volume_db;

        /*
         Now we have to play the sounds. We use a feature found in every browser called "webaudio" which is a nice
         interface. The great thing is that we can tell the browser exactly when to play what sounds and it will
         automatically do that at the correct time. If we schedule overlapping sounds, it will also do the mixing for us.
         the @thht/sounds library make this really easy for us. our sine_wave object has a method called start that sends
         the sound to the webaudio interface.

         What we want to do here is to schedule not only one sound but the number of sounds specified in the n_sounds
         parameter. So, lets loop....
         */


        current_delay = 1 + sine_wave.webaudio_context.currentTime; // we start with a delay of 500ms in order to allow this loop to send everything to webaudio...
        let cur_webaudio_node = null; // this becomes important later
           ReactDom.render(<Fixcross/>, this.display_element,);
        for(let idx_snd=0; idx_snd<this.trialinfo.n_sounds; idx_snd++)
        {
            if(idx_snd > 1 & idx_snd < this.trialinfo.n_sounds - 3) {
                cur_webaudio_node = white_noise.start(current_delay + 0.3);
                if (idx_snd == 2){
                    cur_webaudio_node = sine_wave.start(current_delay)
                }
            } else {
                cur_webaudio_node = start_sine.start(current_delay);
            }
            current_delay += white_noise.duration + 0.3;
        }
        /*
         Now everything is sent to the browser and starts playing.
         One of the most important things one needs to understand in javascript is that most things happen
         "asynchronously". This means that many functions that do stuff return before the job is done.
         In this case, the for loop does not wait until all the sounds have been played. It exits when all of them
         are sent to the browser.

         jspsych needs to know when everything this plugin wants to do is finished. In order to do this, we
         need to call the following function once we are done:

         jsPsych.finishTrial();

         But if we would call it now, jspsych would think, this trial is already over, although sound is still being played.

         So, we need to wait for the last sound to finish. The trick is, that javascript uses so-called "events" that
         you can attach code to. Remember the "cur_webaudio_node" variable above? This is where we store the object that
         webaudio uses for our sounds. Each time we call the "start" method, it returns a new object so at this point in
         the code, "cur_webaudio_node" contains the webaudio object of the last sound that is played.

         This object has an "onended" property. If we assign a function to it, this function gets called when this
         particular sound has stopped playing....
         */
        cur_webaudio_node.onended = () => {this._end_trial()};
    }

    _end_trial() {
        ReactDom.unmountComponentAtNode(this.display_element);
        jsPsych.finishTrial()
    }

}

export default ContinuousTest
