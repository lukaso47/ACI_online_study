import { MLThresholdHunter } from '@thht/ml_threshold';
import ReactDom from 'react-dom';
import React from 'react';
import { Question } from './components/question.jsx';
import { Sine, Noise } from "@thht/sounds";
import {Fixcross} from "../../lib/react_components/fixcross";


class ACISoundThresholdPlugin {
    constructor(jsPsych) {
        this.threshold_hunter = null;
        this.trialinfo = null;
        this.display_element = null;
        this.sound = null;
        this.data = {
            trial_arguments: null,
            individual_trials: [],
            result: {},
        };
        this.jsPsych = jsPsych;

        this.current_trial_data = {};
    }

    get info() {
        return {
            name: 'aci_sound_threshold',
            parameters: {
                aud_thres: {
                    type: this.jsPsych.plugins.parameterType.FLOAT,
                    default: undefined,
                },
                n_sounds: {
                    type: this.jsPsych.plugins.parameterType.INT,
                    default: undefined,
                },
                discomfort_level: {
                    type: this.jsPsych.plugins.parameterType.FLOAT,
                    default: undefined,
                },
                yes_response: {
                    type: this.jsPsych.plugins.parameterType.STRING,
                    default: 'Yes',
                },
                no_response: {
                    type: this.jsPsych.plugins.parameterType.STRING,
                    default: 'No',
                },
                question: {
                    type: this.jsPsych.plugins.parameterType.STRING,
                    default: 'Did you hear the sound?',
                },
            },
        };
    }

    trial(display_element, trial) {
        this.data = {
            trial_arguments: null,
            individual_trials: [],
            result: {},
        };
        this.trialinfo = trial;
        this.display_element = display_element;
        this.data.trial_arguments = trial;

        this.stim_level = this.trialinfo.aud_thres + 32
        let minSNR = this.stim_level - (this.trialinfo.discomfort_level - 1) ;
        if (minSNR < -30){
            minSNR = -30;
        }
        let maxSNR = -5;
        let stepSize = (maxSNR - minSNR)/20
        let conditions = [];
        let snrs = [];
        for (let cur_db = -maxSNR; cur_db <= -minSNR; cur_db = cur_db + stepSize) {
            snrs.push(cur_db)
        }

        for (let idx_rep = 0; idx_rep < 4; idx_rep++) {
            for (let cur_db = -maxSNR; cur_db <= -minSNR; cur_db = cur_db + stepSize) {
                conditions.push(cur_db)
            }
        }
        let shuffle = require('knuth-shuffle').knuthShuffle;
        conditions = shuffle(conditions);
        this.test = conditions;



        this.threshold_hunter = new MLThresholdHunter(conditions[0],
            snrs,
            [0],
            [0.0200, 0.0334, 0.0557, 0.0928, 0.1549, 0.2583, 0.4309, 0.7188, 1.1990, 2.0000],
            this.test.length + 12 ,
            this.test.length + 40,
            40);
        this.play_sound();
    }

    play_sound() {

        const white_noise = new Noise(0.1);
        let probe;
        if (this.threshold_hunter.n_trials < this.test.length) {

            probe = this.test[this.threshold_hunter.n_trials];
        }
        else probe = this.threshold_hunter.current_probe_value;

        white_noise.db = this.stim_level + probe;
        white_noise.apply_bandstop(1673,1793,1000);
        white_noise.apply_sin_ramp(0.005);


        const sine_wave = new Sine(1732, 0.3);
        sine_wave.apply_sin_ramp(0.005); // to avoid clicking
        sine_wave.db = this.stim_level;

        let current_delay = 1 + sine_wave.webaudio_context.currentTime;
        let cur_webaudio_node = null;
        ReactDom.render(<Fixcross/>, this.display_element,);
        for(let idx_snd=0; idx_snd<this.trialinfo.n_sounds; idx_snd++)
        {

            cur_webaudio_node = sine_wave.start(current_delay);
            if (idx_snd > 1 && idx_snd < this.trialinfo.n_sounds - 3){
            cur_webaudio_node = white_noise.start(current_delay+  0.3);}
            current_delay += 0.3 + 0.1;
        }

        cur_webaudio_node.onended = () => this.show_question(this);
    }

    show_question(self) {
        ReactDom.render(
            <Question
                on_response={(response) => self.evaluate_response(response)}
                question={self.trialinfo.question}
                yes_response={self.trialinfo.yes_response}
                no_response={self.trialinfo.no_response}
            />,
            self.display_element,
        );
    }

    evaluate_response(response) {
        let snr;
        if (this.threshold_hunter.n_trials < this.test.length) {
            snr = this.test[this.threshold_hunter.n_trials];
        } else {
            snr = this.threshold_hunter.current_probe_value;
        }

        const cur_data = {
            n_trial: this.threshold_hunter.n_trials,
            cur_std: this.threshold_hunter.std_last_trials,
            answer: response === this.trialinfo.yes_response,
            current_guess: this.threshold_hunter.current_guess,
            current_probe_value: this.threshold_hunter.current_probe_value,
            current_slope: this.threshold_hunter.best_function.slope,
            real_probe_value: snr,
        };

        this.data.individual_trials.push(cur_data);
        this.threshold_hunter.process_response(response === this.trialinfo.yes_response, snr);

        if (this.threshold_hunter.stop) {
            this.data.result.final_estimate = this.threshold_hunter.current_guess;
            this.data.result.converged = this.threshold_hunter.converged;
            this.data.result.n_trials = this.threshold_hunter.n_trials;
            this.data.result.psy_function = this.threshold_hunter.best_function;
            this._end_trial();
        } else {
            this.play_sound();
        }
    }

    _end_trial() {
        ReactDom.unmountComponentAtNode(this.display_element);
        //delete this.data.trial_arguments.sound;
        this.jsPsych.finishTrial(this.data);
    }
}

export default ACISoundThresholdPlugin;
