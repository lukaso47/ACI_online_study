import React from 'react';
import PropTypes from 'prop-types';
import {
    Container, Header, Button,
} from 'semantic-ui-react';

class Question extends React.Component {
    constructor(props) {
        super(props);

        this.handle_response = this.handle_response.bind(this);
    }

    handle_response(evt, val) {
        this.props.on_response(val.children);
    }

    render() {
        return (
            <Container>
                <Header>{this.props.question}</Header>
                <Container>
                    <Button onClick={this.handle_response}>{this.props.yes_response}</Button>
                    <Button onClick={this.handle_response}>{this.props.no_response}</Button>
                </Container>
            </Container>
        );
    }
}

Question.propTypes = {
    no_response: PropTypes.string.isRequired,
    on_response: PropTypes.func.isRequired,
    question: PropTypes.string,
    yes_response: PropTypes.string.isRequired,
};

export { Question };
