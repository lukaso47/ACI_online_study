/*
The problem is that jsPsych is great as a basic framework. But more complex things are not done in a very clever way
when using their default plugins. The best way to get around this is to code a custom plugin that basically contains
the code for a whole trial or even a whole run.

This is what we will do here. And we are going to use some of the more advanced features of javascript, mainly
we are going to code the plugin as a class rather than using a closure.
*/


/*
You should already be familiar with this. We import jspsych here and the Plugin class from the other js file
in the same folder as this index file.
 */
import 'jspsych';
import PresentDynamicSounds from "./present_dynamic_sounds";

// and then we just tell jspsych that we created a new plugin...
jsPsych.plugins.present_dynamic_sounds = new PresentDynamicSounds();
