import "jspsych";
import get_browser from "./parts/browser";
import get_instructions from "./parts/instructions";
import get_consent from "./parts/consent";
import get_demographics from "./parts/dem";
import get_hearing_incidents from "./parts/hearing_incidents";
import get_mini_tq from "./parts/mini_tq";
import get_headphones from "./parts/headphones";
import get_LSHS from "./parts/LSHS";
import get_SL from "./parts/SL";
import perfect_test_trial from "./parts/noiseAndSound_perfect_test";
import discontinous_test_trial from "./parts/noiseAndSound_discontinous_test";
import get_Discomfort from "./parts/discomfortLevel";
import get_Discomfort2 from "./parts/discomfortLevel2";
import get_Discomfort3 from "./parts/discomfortLevel3";
import get_language from "./parts/get_language";
import get_fullscreen from "./parts/fullscreen";
import get_TSCHQ from "./parts/TSCHQ_short";
import { Sine } from "@thht/sounds";
import { init_sound_threshold_plugin } from "@thht_jspsych/sound_threshold";
import { init_aci_threshold_plugin} from "./plugins/ACI_MLThreshold";
//start
import page_1_de from './parts/assets/instructions/instructions_1/page_1_de.html';
import page_1_en from './parts/assets/instructions/instructions_1/page_1_en.html';
//instructions1
import page_2_de from './parts/assets/instructions/instructions_1/page_2_de.html';
import page_2_en from "./parts/assets/instructions/instructions_1/page_2_en.html";
//instructions2
import page_3_de from './parts/assets/instructions/instructions_1/page_3_de.html';
import page_3_en from "./parts/assets/instructions/instructions_1/page_3_en.html";
//instructions3
import page_4_de from './parts/assets/instructions/instructions_1/page_4_de.html';
import page_4_en from "./parts/assets/instructions/instructions_1/page_4_en.html";
//instructions4
import page_5_de from './parts/assets/instructions/instructions_1/page_5_de.html';
import page_5_en from "./parts/assets/instructions/instructions_1/page_5_en.html";
//finish
import aci_thres from "./parts/aci_threshold";


/*
Using "var" to declare variables is considered bad practise nowadays because it can lead to strange behavior.
Use "let" for variables that change and "const" for variables that are constants.
 */
init_aci_threshold_plugin(jsPsych);
init_sound_threshold_plugin(jsPsych);

const n_sound = 14;
const probe_sound = new Sine(1732,1);


jsPsych.init({
    timeline: [
        get_language(),
        get_fullscreen(),
        get_browser(),
        get_instructions(page_1_de,page_1_en),
        get_instructions(page_2_de,page_2_en),
        get_consent(),
        get_demographics(),
        get_instructions(page_3_de,page_3_en),
        get_hearing_incidents(),
        get_mini_tq(),
        get_TSCHQ(),
        get_LSHS(),
        get_instructions(page_4_de,page_4_en),
        get_SL(probe_sound),
        get_headphones(),
        get_Discomfort(),
        get_Discomfort2(),
        get_Discomfort3(),
        get_instructions(page_5_de,page_5_en),
        perfect_test_trial(n_sound),
        discontinous_test_trial(n_sound),
        aci_thres(n_sound)
    ],
    on_finish: function() {
        var resultJson = jsPsych.data.get().json();
        console.log("Finish run");
        jatos.submitResultData(resultJson, jatos.startNextComponent);
    }
});




