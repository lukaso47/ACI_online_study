import i18n from "i18next";
import { initReactI18next} from "react-i18next";
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import resources from '../locales';

const Languages = ['en', 'de'];


i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        fallbackLng: 'en',
        debug: true,
        resources: resources, // from Thomas
        whitelist: Languages,
        interpolation: {
            escapeValue: false,
        }
    });

export default i18n;

