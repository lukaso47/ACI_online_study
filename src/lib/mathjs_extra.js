/*jshint esversion: 6 */
import {matrix} from "mathjs";

function fliplr(mat) {

    "use strict";

    const flip_array = function(array) {
        if(Array.isArray(array[0])) {
            const new_array = [];
            array.forEach(function (el) {
                new_array.push(flip_array(el));
            });

            return new_array;
        }
        else {
            return array.reverse();
        }
    };

    const mat_array = mat.toArray();
    const flipped_array = flip_array(mat_array);

    return matrix(flipped_array);
}

export default {
    fliplr
};
