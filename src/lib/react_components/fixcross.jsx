import React from 'react';
import { Icon } from 'semantic-ui-react';

function Fixcross() {
    return (
        <Icon
            name='plus'
            size='small'
        />
    );
}

export { Fixcross };
