import "jspsych";
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'
import 'jspsych/plugins/jspsych-html-button-response'


export default function get_SL(probe_sound) {



    let SL =  {
        type: 'sound_threshold',
        sound: () => probe_sound,
        db_range: [-120, -20],



        on_start: function (SL) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                SL.question = 'Did you perceive the tone?';
                SL.yes_response = 'Yes, I did';
                SL.no_response = 'No, I did not';
            } else {
                SL.question = 'Haben sie den Ton wahrgenommen?';
                SL.yes_response = 'Ja, habe ich';
                SL.no_response = 'Nein, habe ich nicht';
            }
        },
        on_finish: function(){
        let data = JSON.parse(jsPsych.data.get().json());
           let threshold = JSON.parse(data[data.length - 1].result.final_estimate);
           jsPsych.data.addProperties({aud_thresh: threshold});
        }
    };

    let inst = {
        type: 'html-button-response',
        stimulus: 'abc',
        choices: ['Start'],

        on_start: function(inst){
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                inst.stimulus = 'That did not work. Please turn up your volume to maximum level and press Start to try again!';
            } else {
                inst.stimulus = 'Das hat nicht funktioniert. Bitte stellen sie ihre Lautstärke auf das Maximum und drücken sie Start, um es erneut zu probieren!';
            }
        },

    }
    let if_thresholdhigh = {
        timeline:[inst, SL],
        conditional_function: function() {
            let data = JSON.parse(jsPsych.data.get().json());
            if (JSON.parse(data[data.length - 1].aud_thresh) > -46) {
            return true
        } else{
                return false
            }
        }
    }

    return {
                timeline: [SL, if_thresholdhigh]
    }
}



