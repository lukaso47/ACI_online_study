import 'jspsych';
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'
import '../plugins/white_noise_play'
import 'jspsych/plugins/jspsych-html-button-response'



export default function get_Discomfort2() {

    let discomfortIntensity2 = [];
    for (let iIntensity2 = 0; iIntensity2 < 20; iIntensity2++){
        discomfortIntensity2[iIntensity2] = {added_volume: 2 * (iIntensity2) - 3}
    }
    var disco2 =
        {
            type: 'white_noise_play',
            n_sounds: 1,
            noise_length: 0.1,
            volume_db_noise: -30,
            add_db: jsPsych.timelineVariable('added_volume'),
            isi: 0,
            on_start: function (disco2) {
                var vol_data = JSON.parse(jsPsych.data.getLastTimelineData().json());
                disco2.volume_db_noise = vol_data[vol_data.length-1].volume;
            },
        };

    let discomfort= {
        type: 'html-button-response',
        stimulus: '',
        choices: 'abs',
        on_finish: function () {
            jsPsych.data.get().addToLast({added_volume: jsPsych.timelineVariable('added_volume', true)});
            let data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            if (JSON.parse(data[data.length - 1].button_pressed) == "0") {
                jsPsych.endCurrentTimeline();
                jsPsych.data.addProperties({volume2: (data[data.length - 1].volume + data[data.length - 1].added_volume)});
            }
        },
        on_start: function (discomfort) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                discomfort.choices = ["YES, too loud","NO, NOT too loud"];
                discomfort.stimulus = ["Did the noise have an uncomfortable volume?"];
            } else {
                discomfort.choices = ["JA, zu laut", "NEIN, NICHT zu laut"];
                discomfort.stimulus = ["War das Rauschen unangenehm laut?"];
            }
        }
    };

    return{
        timeline: [
            disco2,
            discomfort,
        ],
        timeline_variables: discomfortIntensity2
    }
}
