import {MLThresholdHunter} from '@thht/ml_threshold'
import 'jspsych';
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'
import 'compute-logspace'
import 'mathjs'
import 'knuth-shuffle'
import '../plugins/ACI_MLThreshold'
export default function aci_thres(n_sound) {



let one_trial = {
    type: "aci_sound_threshold",
    n_sounds: n_sound,
    aud_thres: -60,
    discomfort_level: -20,
    yes_response: "",
    no_response:"",
    question:"",
    on_start(one_trial){
        let data = JSON.parse(jsPsych.data.get().json());
        one_trial.aud_thres = data[data.length - 1].aud_thresh;
        one_trial.discomfort_level = data[data.length - 1].final_vol;
            if ("English" === JSON.parse(data[0].responses).Q0) {
                one_trial.question = 'Did you perceive the tone as continuous when the noise was presented?';
                one_trial.yes_response = '   Continuous   ';
                one_trial.no_response = '   Discontinuous   ';
            } else {
                one_trial.question = 'War der Ton durchgängig, wenn das Rauschen präsentiert wurde?';
                one_trial.yes_response = '   Durchgängig   ';
                one_trial.no_response = '   Unterbrochen   ';
            }
        },
    }

let instruction_before_start = {
        type: 'html-button-response',
        stimulus: ['Drücken sie Start, um mit dem Experiment zu beginnen! Die Studie wird danach automatisch beendet.'],
        choices: ['Start'],
    on_start: function (instruction_before_start){
            let data = JSON.parse(jsPsych.data.get().json());
        if ("English" === JSON.parse(data[0].responses).Q0) {
            instruction_before_start.stimulus = ['Press start to begin the experiment. The study will finish automatically at the end.']
        }
    }
    };

    return {
        timeline: [one_trial]
    };
}