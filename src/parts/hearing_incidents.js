/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';

export default function get_hearing_incidents() {

    // "use strict";

    const options_1 = [ 'Ja', 'Nein'];
    const options_2 = [ 'Yes', 'No'];


    var instructions= {
        type: 'survey-multi-choice',
        preamble: '',
        questions: [{prompt: "abc", options: options_2,required: true},{prompt: "abc", options: options_2,required: true}],
        button_label: '',
        randomize_question_order: false,
        on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_start: function (instructions) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                instructions.questions =  [
                    {prompt: "Have you ever experienced ear noise (tinnitus) in the form of a tone (whistle or peep) or noise (noise, clicking noise, clangor, buzz, whizz) over a longer period of time  ?", options: options_2, required:true, name: 'Q0'},
                    {prompt: "Have you been diagnosed with hearing loss?", options: options_2, required:true, name: 'Q1'}];
                instructions.preamble = '<p style="text-align: left"><u>Clarification of a possible disturbance through tinnitus</u><br> The following questions serve for the clarification of a possible disturbance through tinnitus. Please answer the questions truthfully.</p>';
                instructions.button_label = 'Weiter';
            }   else {
                instructions.questions =  [
                    {prompt: "Haben Sie je über einen längeren Zeitraum Ohrgeräusche (Tinnitus) in Form eines Tons (Pfeifen oder Piepsen) oder atonale Geräusche (Rauschen, Knacken, Klirren, Brummen, Zischen) wahrgenommen?", options: options_1, required:true, name: 'Q0'},
                    {prompt: "Wurde bei Ihnen ein Hörverlust diagnostiziert?", options: options_1, required:true, name: 'Q1'}];
                instructions.preamble = '<p style="text-align: left"><u>Abklärung möglicher Beeinträchtigung durch Tinnitus</u><br> Die folgenden Fragen dienen zur Abklärung einer möglichen Belastung durch Tinnitus. Bitte beantworten Sie die Fragen so gut und wahrheitsgemäß wie möglich.</p>';
                instructions.button_label = 'Weiter';
            }
        }
    };


    return {
        timeline: [instructions]

    };
}