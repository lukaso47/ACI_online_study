import 'jspsych';
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'

export default function discontinous_test_trial(n_sound) {
    "use strict";

    var  discontinous = {
        type: 'present_dynamic_sounds',
        n_sounds: n_sound,
        freq: 1732,
        volume_db: -51,
        volume_db_noise: -50,
        add_db: -10,
        noise_length: 0.1,
        on_start: function (discontinous) {
            let test_data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            discontinous.volume_db_noise = test_data[test_data.length-1].aud_thresh + 27;
            discontinous.volume_db = test_data[test_data.length-1].aud_thresh + 32;
        }
    };

    let perfectinstrus= {
        type: 'html-keyboard-response',
        stimulus:'',
        choices: jsPsych.NO_KEYS,
        trial_duration:9000,
        on_start: function (perfectinstrus) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                perfectinstrus.stimulus= '<p>In 10 seconds you will hear a discontinuous sequence. Please also rate this sequence for training purpose</p>';
            }   else {
                perfectinstrus.stimulus= '<p>In 10 Sekunden werden sie einen unterbrochenen Durchgang hören. Bitte beurteilen Sie diesen später zu Übungszwecken.</p>';
            }
        }
    };

    let perfectresp= {
        type: 'html-button-response',
        stimulus: '',
        choices: ['a'],
        on_start: function (perfectresp) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                perfectresp.choices = ["Continuous", "Discontinuous"];
                perfectresp.stimulus = "How was the target tone like, when the noise the presented?";
            } else {
                perfectresp.choices = ["Durchgängig", "Unterbrochen"];
                perfectresp.stimulus = " Wie war der Zielton, wenn das Rauschen präsentiert wurde?";
            }
        }
    };

    let startexp= {
        type: 'html-button-response',
        stimulus: '',
        choices: ['a'],
        on_start: function (startexp) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                startexp.choices = ["Start"];
                startexp.stimulus = 'If you press the "Start"-button the experiment will begin! <br>' +
                    '<u>Please remember to focus on the tone.' +
                    'After each trial only rate the continuity of the tone and NOT the noise sound!</u> <br>'+
                    'After completion the experiment will close automatically';
            } else {
                startexp.choices = ["Start"];
                startexp.stimulus = 'Wenn Sie auf den "Start"-Button klicken beginnt das Experiment!<br>' +
                    '<u> Bitte fokussieren Sie sich auf den Ton!' +
                    'Im Anschluss an jeden Durchgang bitten wir Sie nur den Ton zu beurteilen und NICHT das Rauschen</u> <br>'+
                    'Nach Abschluss wird das Experiment automaitsch beendet';
            }
        }
    };

    return {
        timeline: [
            perfectinstrus,
            discontinous,
            perfectresp,
            startexp,
        ]
    };
}