/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import demographics_en from './assets/demographics/demographics_en.html';
import demographics_de from './assets/demographics/demographics_de.html';


export default function get_demographics() {


    "use strict";

    let instructions = {
        type: 'survey-html-form',
        html: demographics_de,
        preamble: '',
        button_label: '',
        on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        },
        on_start: function (instructions) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                instructions.html = demographics_en;
                instructions.preamble =
                    '<p style="text-align: left"><u>Demographic Information</u><br><br>The following data ensures a representative random sample of the overall population. The first- and maidname of your mother serves for the generation of an anonymized participant code. ' +
                    'Your e-mail is <u>not mandatory</u> for the participation in the study we would still ask you to enter it just to make sure we are able to contact you for queries. </br><br><br></p>';
                instructions.button_label = 'Next';
            } else {
                instructions.html = demographics_de;
                instructions.preamble =
                    '<p style="text-align: left"><u>Demographische Informationen</u><br><br>Folgende Daten dienen zur Sicherstellung einer repräsentativen Zufallsstichprobe aus der Gesamtbevölkerung. Der Vor- und Mädchenname Ihrer Mutter dient hierbei zur Erstellung Ihres anonymisierten Versuchspersonencodes. ' +
                    'Die Eingabe Ihrer E-mail Adresse ist für die Studienteilnahme <u>nicht verpflichtend</u>, wir würden Sie dennoch darum bitten diese anzugeben, um sie für eventuelle Rückfragen kontaktieren zu können. </br><br><br></p>';
                instructions.button_label = 'Weiter';
            }
        }
    };


    return {
        timeline: [instructions]

    };
}
