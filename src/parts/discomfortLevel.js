import 'jspsych';
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'
import '../plugins/white_noise_play'
import 'jspsych/plugins/jspsych-html-button-response'

// How are discomfortLevel1-3 different from each other? The code seems quite similar....

export default function get_Discomfort() {
    let iIntensity;
    let discomfortIntensity = [];
    for (iIntensity = 0; iIntensity < 25; iIntensity++){
        discomfortIntensity[iIntensity] = {add_db: 3 * iIntensity}
    }



    let disco1 = {
        type: 'white_noise_play',
        n_sounds: 1,
        noise_length: 0.1,
        volume_db_noise: -10,
        add_db: jsPsych.timelineVariable('add_db'),
        isi: 0,
        on_start: function (disco1){
            let data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            this.volume_db_noise = data[data.length-1].aud_thresh + 32
            if(data[data.length-1].aud_thresh + 32 + jsPsych.timelineVariable('add_db', true) > -1){
                this.add_db = jsPsych.timelineVariable('add_db', true) - 3;
                jsPsych.data.get().addToLast({clipping: 1})}
                else {
                jsPsych.data.get().addToLast({clipping: 0})}
            }
        }

    let discomfort= {
        type: 'html-button-response',
        stimulus: "Did the noise have an uncomfortable volume?",
        choices: ['a'],
        on_finish: function () {
            let data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            if (JSON.parse(data[data.length - 1].button_pressed) == "0") {
                jsPsych.data.addProperties({volume: data[data.length - 1].aud_thresh + 32 + jsPsych.timelineVariable("add_db", true)});
                jsPsych.endCurrentTimeline();
            } else if (data[data.length - 1].aud_thresh + 32 + jsPsych.timelineVariable('add_db', true) >= -3) {
                jsPsych.data.get().addToLast({clipping: 1})
                jsPsych.endCurrentTimeline();
            }
                else {
                    jsPsych.data.get().addToLast({clipping: 0})}
            },
        on_start: function (discomfort) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"==JSON.parse(data[0].responses).Q0){
                discomfort.choices = ["YES, too loud","NO, NOT too loud"];
                discomfort.stimulus = ["Did the noise have an uncomfortable volume?"];
            }   else {
                discomfort.choices = ["JA, zu laut", "NEIN, NICHT zu laut"];
                discomfort.stimulus = ["War das Rauschen unangenehm laut?"];
            }
        }
    };

   let whole_dis = {
       timeline:[disco1,discomfort],
       timeline_variables: discomfortIntensity
   };


    let inst = {
        type: 'html-button-response',
        stimulus: 'abc',
        choices: ['Start'],

        on_start: function (inst) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                inst.stimulus = 'That did not work. Please turn up your volume to maximum level and press Start to try again!';
            } else {
                inst.stimulus = 'Das hat nicht funktioniert. Bitte stellen sie ihre Lautstärke auf das Maximum und drücken sie Start, um es erneut zu probieren!';
            }
        },
    }

        let if_clip = {
            timeline: [inst, whole_dis],
            conditional_function: function () {
                let data = JSON.parse(jsPsych.data.get().json());
                if (data[data.length -1].clipping == 1) {
                    return true
                }
                else {
                    return false
                }
            }
        }

    return{

        timeline: [whole_dis, if_clip]
    }
}




