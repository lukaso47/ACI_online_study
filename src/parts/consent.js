/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import consent_de from './assets/demographics/consent_de.html';
import consent_en from './assets/demographics/consent_en.html';



export default function get_consent() {

    "use strict";

    let instructions= {
        type: 'survey-html-form',
        html: consent_de,
        preamble: '',
        button_label: '',
        on_start: function (instructions) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                instructions.html = consent_en;
                instructions.button_label = 'I want to participate';
                instructions.preamble = '<p style="font-size:30px"><b>Consent form</b><br><br></p>';
            }   else {
                instructions.html = consent_de;
                instructions.button_label = 'Ich möchte teilnehmen';
                instructions.preamble = '<p style="font-size:30px"><b>Einverständniserklärung</b><br><br></p>';
            }
        }
    };


    return {
        timeline: [instructions]

    };
}
