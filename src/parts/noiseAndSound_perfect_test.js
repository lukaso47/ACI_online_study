import 'jspsych';
import '../plugins/continuous_trial'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'

export default function perfect_test_trial(n_sound){
    "use strict";
    var  continuous = {
        type: "continuous_test",
        n_sounds: n_sound,
        snd_length: n_sound * 0.4,
        freq: 1732,
        volume_db: -65,
        volume_db_noise: -45,
        add_db: 0,
        noise_length: 0.1,
        on_start: function (continuous) {
            let test_data = JSON.parse(jsPsych.data.get().json());
            continuous.volume_db_noise = test_data[test_data.length - 1].aud_thresh + 42;
            continuous.volume_db = test_data[test_data.length - 1].aud_thresh + 32;
        }
    };

    let perfectinstrus= {
        type: 'html-keyboard-response',
        stimulus:'',
        choices: jsPsych.NO_KEYS,
        trial_duration:8000,
        on_start: function (perfectinstrus) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                perfectinstrus.stimulus= '<p>In 10 seconds you will hear a continuous sequence. Please also rate this sequence for training purpose.</p>';
            }   else {
                perfectinstrus.stimulus= '<p>In 10 Sekunden werden sie einen durchgängigen Durchgang hören. Bitte beurteilen Sie diesen später zu Übungszwecken.</p>';
            }
        }
    };

    let perfectresp= {
        type: 'html-button-response',
        stimulus: '',
        choices: ['a'],
        on_start: function (perfectresp) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                perfectresp.choices= ["Continuous", "Discontinuous"];
                perfectresp.stimulus= "How was the target tone like, when the noise was presented?";
            }   else {
                perfectresp.choices= ["Durchgängig","Unterbrochen"];
                perfectresp.stimulus= " Wie war der Zielton, wenn das Rauschen präsentiert wurde?";
            }
        }
    };


    return {
        timeline: [
            perfectinstrus,
            continuous,
            perfectresp,
        ],
    };
}