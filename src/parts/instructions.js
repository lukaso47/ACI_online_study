import 'jspsych';
import 'jspsych/plugins/jspsych-instructions';




export default function get_instructions(pageDE, pageEN) {


    let instructions = {
        type: 'instructions',
        pages: [],
        show_clickable_nav: true,
        button_label_previous: '',
        button_label_next: '',
        on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        },


        on_start: function (instructions) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                instructions.pages = [pageEN];
                instructions.button_label_previous = 'Back';
                instructions.button_label_next = 'Next';
            } else {
                instructions.pages = [pageDE];
                instructions.button_label_previous = 'Zurück';
                instructions.button_label_next = 'Weiter';
            }
        }
    };


    return {
        timeline: [instructions]

    };
}
