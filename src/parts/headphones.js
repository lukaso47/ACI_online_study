/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-image-button-response';


import img_headphones_de from './assets/images/Headphone_pic_de.png';
import img_headphones_en from './assets/images/Headphone_pic_en.png';

export default function get_headphones() {
    "use strict";

    let headphones_en = {
        type: 'image-button-response',
        stimulus: img_headphones_en,
        choices: ['Over-Ear headphones', 'In-Ear headphones'],

    };

    let startexp_en = {
        type: 'html-button-response',
        stimulus: 'If you press the "Start"-button the discomfort measurement will begin!',
        choices: ["Start"],
    };

    let if_english = {
        timeline: [headphones_en, startexp_en],
        conditional_function: function () {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0)
                return true
            else return false
        }

    };

    let headphones_de = {
        type: 'image-button-response',
        stimulus: img_headphones_de,
        choices: ['Over-Ear Kopfhörer', 'In-Ear Kopfhörer'],
    };

    let startexp_de = {
        type: 'html-button-response',
        stimulus: ['Wenn Sie auf den "Start"-Button klicken beginnt die Bestimmung der Unbehaglichkeitsgrenze!'],
        choices: ['Start']
    };

    let if_deutsch = {
        timeline: [headphones_de, startexp_de],
        conditional_function: function () {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" !== JSON.parse(data[0].responses).Q0)
                return true
            else return false
        }

    };

    return {
        timeline: [if_english, if_deutsch]
    }
}