import 'jspsych';
import '../plugins/present_dynamic_sounds_example'
import 'jspsych/plugins/jspsych-html-keyboard-response'
import 'jspsych/plugins/jspsych-survey-multi-choice'
import {range} from 'mathjs'


export default function trials( conditions) {
    "use strict";


    /*function generateRangeStim(){
        console.log('Eval func')
            let data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            let tone_level = data[data.length - 1].aud_thresh + 10;
            let discomfort_level = data[data.length - 1].final_vol;
            let minSNR = tone_level + ((discomfort_level - 1) - tone_level);
            let maxSNR = 15;
            let step = (maxSNR - minSNR) / 8;
            let conditions = [];
            for (let idx_rep = 0; idx_rep < 10; idx_rep++) {
                for (let cur_db = -maxSNR; cur_db <= -minSNR; cur_db = cur_db + step) {
                    conditions.push(
                        {add_db: cur_db}
                    )
                }
            }
            return conditions
    }*/


    let one_trial = {
        type: 'present_dynamic_sounds',
        n_sounds: 12,
        freq: 1732,
        volume_db: -51,
        volume_db_noise: -50,
        add_db: jsPsych.timelineVariable('add_db'),
        noise_length: 0.1,
        on_start: function (one_trial) {
            let test_data = JSON.parse(jsPsych.data.getLastTimelineData().json());
            one_trial.volume_db_noise = test_data[test_data.length - 1].aud_thresh +30;
            one_trial.volume_db = test_data[test_data.length - 1].aud_thresh +30;
        }
    };

    let response = {
        type: 'html-button-response',
        stimulus: '',
        choices: ['a'],
        on_start: function (response) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                response.choices = ["Discontinuous", "Rather discontinuous", "Rather continuous", "Continuous"];
                response.stimulus = "How was the target tone like when noise was presented?";
            } else {
                response.choices = ["Unterbrochen", "Eher Unterbrochen", "Eher Durchgehend", "Durchgehend"];
                response.stimulus = " Wie war der Zielton, als Rauschen präsentiert wurde?";
            }
        },
        on_finish: function () {
            jsPsych.data.get().addToLast({condition: jsPsych.timelineVariable('add_db', true)})
        }
    };
    var trials = {
        timeline: [one_trial, response],
        timeline_variables: conditions,
        randomize_order: true,
    };






    let end_text = {
        type: 'html-button-response',
        stimulus: '',
        choices: ['a'],
        on_start: function (end_text) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English" === JSON.parse(data[0].responses).Q0) {
                end_text.choices = ['Continue'];
                end_text.stimulus = ['You are almost done! Press continue to get to the last part of the experiment, where we will assess the threshold of your perception of continuity. Afterwards, the study will finish automatically. '];
            } else {
                end_text.choices = ['Weiter'];
                end_text.stimulus = ['Sie haben es fast geschafft! Im letzen Teil erfassen wir den Grenzwert, wenn sie den Ton als durchgängig wahrnehmen. Danach wird die Studie automatisch beendet!'];
            }
        },
        on_finish: function () {
            jsPsych.data.get().addToLast({condition: jsPsych.timelineVariable('add_db', true)})
        }
    };

    return {
     timeline:[trials,end_text]
    }


}