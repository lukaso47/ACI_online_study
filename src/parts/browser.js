/*jshint esversion: 6 */
import "jspsych";
import "../plugins/browser_check/detect_browser";


export default function get_browser(){

    /*
    this is not necessary when using webpack...
     */
    "use strict";

    return{
        timeline: [
            {
                type: 'detect_browser',
                platform: ['desktop'],
                //os: undefined,
                browser: ['Firefox', 'Chrome'],
            }
        ]
    };

}

