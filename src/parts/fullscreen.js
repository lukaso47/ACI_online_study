/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';
import 'jspsych/plugins/jspsych-fullscreen'




export default function get_fullscreen() {

"use strict";



let fulls= {
    type: 'fullscreen',
    fullscreen_mode: true,
    message: '',
    button_label: '',
    on_start: function (fulls) {
        let data = JSON.parse(jsPsych.data.get().json());
        if ("English"===JSON.parse(data[0].responses).Q0){
            fulls.message = '<p>The experiment will switch to <b>full screen mode</b> when you press the "Continue" button below.<br><br></p>' +
                '<p style="text-align: left"><b>Please make sure to close all programs and apps that could send notifications or reminders during the experiment (e.g. Mail, Calender). <br>' +
                'Please do also close all other tabs in your browser, if possible!</b></p> ';
            fulls.button_label = 'Continue';
        }   else {
            fulls.message = '<p>Das Experiment wird zum <b>Vollbildmodus</b> wechseln, wenn sie auf "Weiter" klicken. <br><br></p> ' +
                '<p style="text-align: left"><b>Bitte stellen Sie sicher, dass Sie alle Programme, welche Mitteilungen oder Reminder senden könnten, für die Dauer des Expereiments beenden (z.B Mailprogramm, Kalender etc.) <br>' +
                'Bitte schließen Sie wenn möglich auch alle anderen Tabs ihres Internetbrowsers für die gesamte Dauer des Experiments!</b></p>';
            fulls.button_label = 'Weiter';
        }
    }
};


return {
    timeline: [fulls]

};
}