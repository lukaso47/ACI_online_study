/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';

export default function get_LSHS(){

    "use strict";

    //(Bentall & Slade, 1985a, Übersetzung: Lincoln & Keller)

    const options_q1 = ['trifft sicher nicht auf mich zu','trifft eher nicht auf mich zu', 'unsicher', 'trifft eher auf mich zu', 'trifft sicher auf mich zu'];
    const options_q2 = ['certainly does not apply to me', 'rather does not apply to me', 'uncertain', 'rather applies to me', 'certainly applies to me'];

    var LSHS = {type: 'survey-multi-choice',
        preamble: '',
        questions: [],
        randomize_question_order: true,
        button_label : '',
        on_start: function (LSHS) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                LSHS.preamble = '<p style="text-align:left"><u>Launay Slade Hallucination Scale</u><br><br>'+
                    'In the following you will find a series of questions related to the frequency ' +
                    'of certain perceptual phenomena as well as the general psychological state. ' +
                    'Please answer each question as honestly as possible. ' +
                    'If none of the possible answers apply to you, please try to choose the one that fits best for you.</p>';
                LSHS.questions =     [
                    {prompt: "<u>No matter how hard I try to concentrate, unrelated thoughts always creep into my mind.</u>", options: options_q2, required:true, name: 'Q0' },
                    {prompt: "<u>In my daydreams I can hear the sound of a tune almost as clearly as if I were actually listening to it.</u>", options: options_q2, required:true, name: 'Q1'},
                    {prompt: "<u>Sometimes my thoughts seem as real as actual events in my life.</u>", options: options_q2, required:true, name: 'Q2' },
                    {prompt: "<u>Sometimes a passing thought will seem so real that it frightens me.</u>", options: options_q2, required:true, name: 'Q3' },
                    {prompt: "<u>The sounds I hear in my daydreams are generally clear and distinct.</u>", options: options_q2, required:true, name: 'Q4' },
                    {prompt: "<u>The people in my daydreams seem so true to life that sometimes I think they are.</u>", options: options_q2, required:true, name: 'Q5' },
                    {prompt: "<u>I often hear a voice speaking my thoughts aloud.</u>", options: options_q2, required:true, name: 'Q6'},
                    {prompt: "<u>In the past, I have had the experience of hearing a person’s voice and then found that no-one was there.</u>", options: options_q2, required:true, name: 'Q7' },
                    {prompt: "<u>On occasions, I have seen a person’s face in front of me when no-one was in fact there.</u>", options: options_q2, required:true, name: 'Q8' },
                    {prompt: "<u>I have heard the voice of the Devil.</u>", options: options_q2, required:true, name: 'Q9' },
                    {prompt: "<u>In the past, I have heard the voice of God speaking to me.</u>", options: options_q2, required:true, name: 'Q10' },
                    {prompt: "<u>I have been troubled by hearing voices in my head.</u>", options: options_q2, required:true, name: 'Q11' },
                ];
                LSHS.button_label = 'Next';
            }   else {
                LSHS.preamble = '<p style="text-align: left"><u>Launay Slade Hallucination Scale</u><br><br>'+
                    'Im Folgenden finden Sie eine Reihe von Fragen, die sich auf die Autretenshäufigkeit ' +
                    'bestimmter Wahrnehmungsphänomene sowie um psychische Befindlichkeit beziehen.' +
                    'Bitte beantworten Sie jede Frage so ehrlich wie möglich.' +
                    'Sollte es vorkommen, dass keine der vorgegebenen Antwortmöglichkeiten für Sie zutrifft, kreuzen Sie bitte diejenige Antwort an,' +
                    'die noch am ehesten Ihrer Einschätzung entspricht bzw. für Sie zutrifft.</p>' ;
                LSHS.questions =     [
                    {prompt: "<u>Egal, wie stark ich versuche mich zu konzentrieren, es tauchen immer wieder zusammenhangslose Gedanken auf.</u>", options: options_q1, required:true,name: 'Q0'},
                    {prompt: "<u>In meinen Tagträumen kann ich den Klang einer Melodie fast so deutlich hören, als ob ich sie wirklich anhören würde.</u>", options: options_q1, required: true,name: 'Q1' },
                    {prompt: "<u>Manchmal erscheinen meine Gedanken so real wie tatsächliche Ereignisse in meinem Leben.</u>", options: options_q1, required:true,name: 'Q2' },
                    {prompt: "<u>Manchmal erscheint ein vorübergehender Gedanke so real,dass es mich ängstigt.</u>", options: options_q1, required:true,name: 'Q3' },
                    {prompt: "<u>Die Geräusche, die ich in meinen Tagträumen höre, sind normalerweise klar und deutlich.</u>", options: options_q1, required:true,name: 'Q4' },
                    {prompt: "<u>Die Menschen in meinen Tagträumen erscheinen so lebensecht, dass ich manchmal denke, dass sie es wirklich sind.</u>", options: options_q1, required: true, name: 'Q5'},
                    {prompt: "<u>Ich höre oft eine Stimme, die meine Gedanken laut ausspricht.</u>", options: options_q1, required:true,name: 'Q6' },
                    {prompt: "<u>In der Vergangenheit habe ich die Erfahrung gemacht eine menschliche Stimme zu hören und dann festzustellen, dass niemand da war.</u>", options: options_q1, required:true,name: 'Q7' },
                    {prompt: "<u>Gelegentlich habe ich das Gesicht einer Person vor mir gesehen, obwohl in Wirklichkeit niemand da war.</u>", options: options_q1, required:true,name: 'Q8' },
                    {prompt: "<u>Ich habe die Stimme des Teufels gehört.</u>", options: options_q1, required: true,name: 'Q9' },
                    {prompt: "<u>In der Vergangenheit habe ich die Stimme Gottes zu mir sprechen gehört.</u>", options: options_q1, required:true,name: 'Q10' },
                    {prompt: "<u>Es hat mich schon belastet, Stimmen in meinem Kopf zu hören.</u>", options: options_q1, required:true,name: 'Q11' },
                ];
                LSHS.button_label = 'Weiter';
            }
        },
        on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        }};

    return {

        timeline:[

            {
                timeline: [LSHS],

            },

        ]

    };
}