import 'jspsych/plugins/jspsych-survey-multi-choice';


export default function get_language(){

    "use strict";

    return {

        type: 'survey-multi-choice',
        preamble: '<p style="text-align: left"><u>Choose your language/Bitte wählen Sie ihre Sprache</u>',
            questions: [{prompt: "Language/Sprache", options: ['English', 'Deutsch'], required:true, },
        ],
        randomize_question_order: false,
        button_label : 'Next/Weiter',

    };
}





