/*jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';


export default function get_mini_tq() {

    "use strict";

    // Hiller, W. & Goebel, G. (2004): Rapid assessment of tinnitus-related psychological distress using the Mini-TQ;
    // International Journal of Audiology 43: 600-604; Goebel, G. & Hiller, W. (1999): Quality management in the therapy
    // of chronic tinnitus. In: Hazell, J. (Ed.): Proceedings of the sixth International Tinnitus Seminar, Cambridge.
    // Tinnitus and Hyperacusis Center, London: 357- 363

    const options_q1 = ['stimmt', 'stimmt teilweise', 'stimmt nicht'];
    const options_q2 = ['Yes', 'Partly true', 'No'];

    var mini_tq = {type: 'survey-multi-choice',
        preamble: '',
        questions: [],
        randomize_question_order: true,
        button_label : '',
        on_start: function (mini_tq) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                mini_tq.preamble = '<p style="text-align:left"><u>Mini tinnitus questionnaire (Mini-TF12)</u><br><br>'+
                    'The following questions refer to your person, your ear noise as well as possible restrictions occurring due to your ear noise. '+
                    'It is of great importance that you read these questions carefully and answer them completely. There are no right or wrong answers! '+
                    'If none of the possible answers apply to you, please try to choose the one that fits best for you.';
                mini_tq.questions =     [
                    {prompt: "<u>I am aware of my ear noise from the point I wake up until the point I go to bed.</u>", options: options_q2, required:true, name: 'Q0' },
                    {prompt: "<u>I am concerned that there is something seriously wrong in my body due to my ear noise.</u>", options: options_q2, required:true, name: 'Q1'},
                    {prompt: "<u>If the ear noise persists my life will no longer be worth living.</u>", options: options_q2, required:true, name: 'Q2' },
                    {prompt: "<u>I am more petulant when I am with my friends or family because of my ear noise.</u>", options: options_q2, required:true, name: 'Q3' },
                    {prompt: "<u>I am concerned that the ear noise could harm my body.</u>", options: options_q2, required:true, name: 'Q4' },
                    {prompt: "<u>It is hard for me to relax because of my ear noise.</u>", options: options_q2, required:true, name: 'Q5' },
                    {prompt: "<u>My ear noise is often as serious that I am not able to ignore it.</u>", options: options_q2, required:true, name: 'Q6'},
                    {prompt: "<u>Because of my ear noise it takes longer for me to fall asleep.</u>", options: options_q2, required:true, name: 'Q7' },
                    {prompt: "<u>Because of my ear noise I am depressed easily.</u>", options: options_q2, required:true, name: 'Q8' },
                    {prompt: "<u>I often think about if the ear noise will ever disappear again.</u>", options: options_q2, required:true, name: 'Q9' },
                    {prompt: "<u>I am victim of my ear noise.</u>", options: options_q2, required:true, name: 'Q10' },
                    {prompt: "<u>The ear noise does influence my concentration.</u>", options: options_q2, required:true, name: 'Q11' },
                ];
                mini_tq.button_label = 'Next';
            }   else {
                mini_tq.preamble = '<p style="text-align: left"><u>Mini Tinnitus Fragebogen (Mini-TF12)</u><br><br>'+
                    'Im Folgenden finden Sie eine Reihe von Fragen, die sich auf Ihre Person, ' +
                    'Ihre Ohrgeräusche sowie auf mögliche Beeinträchtigungen, die mit Ihren Ohrgeräuschen einhergehen können, beziehen.' +
                    ' Wir bitten Sie, alle Fragen sorgfältig und vollständig zu beantworten. Es gibt keine richtigen oder falschen Antworten!' +
                    ' Sollte es vorkommen, dass keine der vorgegebenen Antwortmöglichkeiten für Sie zutrifft, kreuzen Sie bitte diejenige Antwort an,' +
                    ' die noch am ehesten Ihrer Einschätzung entspricht bzw. für Sie zutrifft.</p>' ;
                mini_tq.questions =     [
                    {prompt: "<u>Ich bin mir der Ohrgeräusche vom Aufwachen bis zum Schlafengehen bewusst.</u>", options: options_q1, required:true,name: 'Q0'},
                    {prompt: "<u>Ich mache mir wegen der Ohrgeräusche Sorgen, ob mit meinem Körper ernstlich etwas nicht in Ordnung ist.</u>", options: options_q1, required: true,name: 'Q1' },
                    {prompt: "<u>Wenn die Ohrgeräusche andauern, wird mein Leben nicht mehr lebenswert sein.</u>", options: options_q1, required:true,name: 'Q2' },
                    {prompt: "<u>Aufgrund der Ohrgeräusche bin ich mit meiner Familie und meinen Freunden gereizter.</u>", options: options_q1, required:true,name: 'Q3' },
                    {prompt: "<u>Ich sorge mich, dass die Ohrgeräusche meine körperliche Gesundheit schädigen könnten.</u>", options: options_q1, required:true,name: 'Q4' },
                    {prompt: "<u>Wegen der Ohrgeräusche fällt es mir schwer, mich zu entspannen.</u>", options: options_q1, required: true, name: 'Q5'},
                    {prompt: "<u>Oft sind meine Ohrgeräusche so schlimm, dass ich sie nicht ignorieren kann.</u>", options: options_q1, required:true,name: 'Q6' },
                    {prompt: "<u>Wegen der Ohrgeräusche brauche ich länger zum Einschlafen.</u>", options: options_q1, required:true,name: 'Q7' },
                    {prompt: "<u>Wegen der Ohrgeräusche bin ich leichter niedergeschlagen.</u>", options: options_q1, required:true,name: 'Q8' },
                    {prompt: "<u>Ich denke oft darüber nach, ob die Ohrgeräusche jemals weggehen werden.</u>", options: options_q1, required: true,name: 'Q9' },
                    {prompt: "<u>Ich bin Opfer meiner Ohrgeräusche.</u>", options: options_q1, required:true,name: 'Q10' },
                    {prompt: "<u>Die Ohrgeräusche haben meine Konzentration beeinträchtigt.</u>", options: options_q1, required:true,name: 'Q11' },
                ];
                mini_tq.button_label = 'Weiter';
            }
        },
        on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        }};

    return {

        timeline:[

            {
                timeline: [mini_tq],
                conditional_function: function () {
                    let data = JSON.parse(jsPsych.data.get().json());
                    if("Ja" === JSON.parse(data[8].responses).Q0 || "Yes" === JSON.parse(data[8].responses).Q0) {
                        return true;
                    } else {
                        return false;
                    }

                }
            },

        ]

    };
}