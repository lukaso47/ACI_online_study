import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';

export default function get_TSCHQ() {

    "use strict";

    var TSCHQ = {
        type: 'survey-multi-choice',
        preamble: '',
        questions: [],
        randomize_question_order: false,
        button_label: 'Weiter',
        on_load: function ()
        {
            document.getElementById("jspsych-content").style.marginTop = "150px";
        },
        on_finish: function ()
        {
            document.getElementById("jspsych-content").style.margin ="auto";
        },
        on_start: function (TSCHQ) {
            let data = JSON.parse(jsPsych.data.get().json());
            if ("English"===JSON.parse(data[0].responses).Q0){
                TSCHQ.preamble = '<p style="text-align: left"> <strong> Tinnitus sample case history questionnaire (TSCHQ) </strong><br><br>'+
                    'In the following questionnaire you will find some questions regarding the characteristics of your tinnitus. '+
                    'It is of great importance that you read these questions carefully and answer them completely. There are no right or wrong answers! '+
                    'If none of the possible answers apply to you please try to choose the one that fits best for you.';
                TSCHQ.questions =     [
                    {prompt: "<u>Family history of tinnitus complaints.</u>",
                        options: ['YES - parents', 'YES - siblings', 'YES - children', 'YES - more than one', 'NO'], required: true,name: 'Q0'},
                    {prompt: "<u>Initial onset. When did you first experience your tinnitus?</u>",
                        options: ['less than 1 month ago', 'less than 3 months ago', 'less than 6 months ago', 'less than 1 year ago', 'less than 3 years ago','less than 5 years ago','less than 10 years ago', 'more than 10 years ago'], required: true,name: 'Q1'},
                    {prompt: "<u>How did you perceive the beginning?</u>",
                        options: ['Gradual','Abrupt'], required: true,name: 'Q2'},
                    {prompt: "<u>Was the onset of your tinnitus related to:</u>",
                        options: ['loud blast of noise','head trauma','stress','whiplash', 'change in hearing', 'other'], required: true,name: 'Q3'},
                    {prompt: "<u>Does your tinnitus seem to pulsate?</u>",
                        options: ['YES - with heart beat', 'YES - different from heart beat','NO'], required: true,name: 'Q4'},
                    {prompt: "<u>Where do you perceive your tinnitus?</u>",
                        options: ['right ear', 'left ear', 'both ears, worse in left','both ears, worse in right','both ears, equally','inside the head','elsewhere'], required: true,name: 'Q5'},
                    {prompt: "<u>How does your tinnitus manifest over time?</u>",
                        options: ['intermittent','constant'], required: true,name: 'Q6'},
                    {prompt: "<u>Describe the loudness of your tinnitus.</u>",
                        options: ['very quiet', 'quiet','rather quiet','rather lout', 'loud','very loud'], required: true,name: 'Q7'},
                    {prompt: "<u>Does your tinnitus sound more like a tone or more like a noise:</u>",
                        options: ['tone','noise','crickets','other'], required: true,name: 'Q8'},
                    {prompt: "<u>Please describe the pitch of your tinnitus:</u>",
                        options: ['very high frequency','high frequency','medium frequency', 'low frequency'], required: true,name: 'Q9'},
                    {prompt: "<u>Is your tinnitus reduced by music or by certain types of environmental sounds such as the noise of a waterfall or the noise of running water when you are standing in the shower ? </u>",
                        options: ['YES', 'NO', 'I do not know'], required: true,name: 'Q10'},
                    {prompt: "<u>Does the presence of loud noise make your tinnitus worse?</u>",
                        options: ['YES','NO', 'I do not know'], required: true,name: 'Q11'},
                    {prompt: "<u>Do you think you have a hearing problem?</u>",
                        options: ['YES','NO'], required: true,name: 'Q12'},
                    {prompt: "<u>Do you have a problem tolerating sounds because they often seem much too loud ? That is, do you often find too loud or hurtful sounds which other people around you find quite comfortable ? </u>",
                        options: ['Never','Rarely','Sometimes','Usually','Always'], required: true,name: 'Q13'},
                ];
                TSCHQ.button_label = 'Next';
            }   else {
                TSCHQ.preamble = '<p style="text-align: left"> <strong> FRAGEBOGEN ZUR CHARAKTERISTIK DES TINNITUS </strong><br><br>' +
                    'Im Folgenden finden Sie einige Fragen die sich auf die Charakteristik Ihres Tinnitus beziehen. '+
                    'Wir bitten Sie, alle Fragen sorgfältig und vollständig zu beantworten. Es gibt keine richtigen oder falschen Antworten! '+
                    'Sollte es vorkommen, dass keine der vorgegebenen Antwortmöglichkeiten für Sie zutrifft, kreuzen Sie bitte diejenige Antwort an,'+
                    ' die noch am ehesten Ihrer Einschätzung entspricht bzw. für Sie zutrifft. </p>' ;
                TSCHQ.questions =     [
                    {prompt: "<u>Gibt es bei Ihnen in der Familie bereits andere Fälle von Tinnitus?</u>",
                        options: ['JA - Eltern', 'JA - Geschwister', 'JA - Kinder', 'JA-Mehrere' , 'NEIN'], required: true, name: 'Q0'},
                    {prompt: "<u>Wann haben Sie zum ersten Mal Tinnitus wahrgenommen?</u>",
                        options: ['Vor weniger als 1 Monat', 'Vor weniger als 3 Monaten', 'Vor weniger als 6 Monaten', 'Vor weniger als 1 Jahr', 'Vor weniger als 3 Jahren', 'Vor weniger als 5 Jahren', 'Vor weniger als 10 Jahren', 'Vor mehr als 10 Jahren'],required: true, name: 'Q1'},
                    {prompt: "<u>Wie hat der Tinnitus bei Ihnen begonnen?</u>",
                        options: ['Schleichend', 'Abrupt'], required: true, name: 'Q2'},
                    {prompt: "<u>Was war der Auslöser Ihres Tinnitus?</u>",
                        options: ['Lautes knallartiges Geräusch', 'Schädel-Hirn Trauma', 'Stress', 'Schleudertrauma', 'Hörverlust oder Änderung des Hörvermögens','keiner der angeführten'], required: true, name: 'Q3'},
                    {prompt: "<u>Nehemen Sie ihren Tinnitus als pulsierend wahr?</u>",
                        options: ['JA - mit meinem Herzschlag', 'JA - aber nicht mit meinem Herzschlag', 'NEIN'], required: true, name: 'Q4'},
                    {prompt: "<u>Wo nehmen Sie ihren Tinnitus wahr?</u>",
                        options: ['Rechtes Ohr', 'Linkes Ohr','In beiden Ohren aber schlimmer im linken','In beiden Ohren aber schlimmer im rechten', 'In beiden Ohren gleichmäßig', 'Im Kopfinneren','Anderswo'], required: true, name: 'Q5'},
                    {prompt: "<u>Wie manifestiert sich Ihr Tinnitus längerfristig gesehen?</u>",
                        options: ['Zeitweise','Dauerhaft'], required: true, name: 'Q6'},
                    {prompt: "<u>Beschreiben Sie die Lautstärke Ihres Tinnitus.</u>",
                        options: ['Sehr leise','leise','eher leise','eher laut','laut','sehr laut'],required: true, name: 'Q7'},
                    {prompt: "<u>Wie nehemen sie die Charakteristik ihres Tinnitus wahr?</u>",
                        options: ['Wie einen Ton','Wie Rauschen','Wie Zirpen (Grille)','Keine der angeführten'], required: true, name: 'Q8'},
                    {prompt: "<u>Bitte beschreiben Sie die Tonhöhe Ihres Tinnitus.</u>",
                        options: ['sehr hoch','hoch','mittel','tief'], required: true, name: 'Q9'},
                    {prompt: "<u>Reduziert sich die Intensität Ihres Tinnitus, wenn Sie Musik hören oder bei Umweltgeräuschen wie bspw. einem Wasserfall oder wenn Sie unter der Dusche stehen?</u>",
                        options: ['JA','NEIN','Ich weiß es nicht'], required: true, name: 'Q10'},
                    {prompt: "<u>Verschlimmern laute Geräusche Ihren Tinnitus?</u>",
                        options: ['JA','NEIN','Ich weiß es nicht'], required: true, name: 'Q11'},
                    {prompt: "<u>Glauben Sie, dass Sie Hörprobleme haben?</u>",
                        options: ['JA','NEIN'], required: true, name: 'Q12'},
                    {prompt: "<u>Empfinden Sie oft Geräusche, welche andere Menschen als nicht störend beurteilen, als zu laut oder schmerzhaft?</u>",
                        options: ['Niemals','Selten','Manchmal','Meistens','Immer'], required: true, name: 'Q13'},
                ];
                TSCHQ.button_label = 'Weiter';
            }
        },
    };

    return {
        timeline: [
            {
                timeline: [TSCHQ],
                conditional_function: function (){
                    let data = JSON.parse(jsPsych.data.get().json());
                    if ("Ja" === JSON.parse(data[8].responses).Q0 || "Yes" === JSON.parse(data[8].responses).Q0){
                        return true;
                    } else {
                        return false;
                    }
                }
            },
        ]
    };
}

