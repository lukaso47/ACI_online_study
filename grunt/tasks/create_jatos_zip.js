const fs = require('fs');
const os = require('os');
const path = require('path');
const fs_extra = require('fs-extra');
const archiver = require('archiver');
const { v5: uuidv5 } = require('uuid');
const glob = require('glob');

module.exports = function(grunt) {
    grunt.registerTask('create_jatos_zip', 'Build zip file for jatos import', function () {
        grunt.task.requires('build');

        const pkg_name = grunt.config.get('pkg.name');

        if(pkg_name === 'your_experiment_name_goes_here')
        {
            throw Error('Please set a proper name in package.json!');
        }

        const done = this.async();

        let tmpdir = os.tmpdir() + path.sep;

        let this_tmp = fs.mkdtempSync(tmpdir);
        let final_dir = path.join(this_tmp, grunt.config.get('pkg.name'));

        fs.mkdirSync(final_dir);

        fs_extra.copySync('dist', final_dir);

        const root_uuid = '00000000-0000-0000-0000-000000000000';

        const uuid_namespace = uuidv5(pkg_name, root_uuid);

        let jatos_jas = {
            version: '3',
            data: {
                uuid: uuidv5(pkg_name, uuid_namespace),
                title: grunt.config.get('pkg.name'),
                description: '',
                groupStudy: false,
                dirName: grunt.config.get('pkg.name'),
                comments: '',
                jsonData: null,
                componentList: []
            }
        };

        glob.sync(path.join(final_dir, '*.html')).forEach(function(html_file) {
            const f_name = path.parse(html_file).base;
            const title = path.parse(html_file).name;

            if(f_name === 'index.html')
            {
                return;
            }

            const this_data = {
                uuid: uuidv5(f_name, uuid_namespace),
                title: title,
                htmlFilePath: f_name,
                reloadable: false,
                active: true,
                comments: "",
                jsonData: null
            };

            jatos_jas.data.componentList.push(this_data);
        });

        let jatos_jas_str = JSON.stringify(jatos_jas);

        fs.writeFileSync(path.join(this_tmp, grunt.config.get('pkg.name') + '.jas'),
            jatos_jas_str);

        let final_zip = fs.createWriteStream('jatos.zip');
        let archive = archiver('zip');
        archive.pipe(final_zip);

        archive.on('error', function (err) {
            throw err;
        });

        final_zip.on('end', function () {
            done();
        });

        archive.directory(this_tmp + path.sep, false);
        archive.finalize();
    })
};
