import 'jspsych';

import get_images_trials from "./parts/show_images";

jsPsych.init({
    timeline: [
        get_images_trials(10)
    ]
});