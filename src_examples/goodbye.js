import 'jspsych';

import get_bye from './parts/bye';

jsPsych.init({
    timeline: [
        get_bye()
    ]
});