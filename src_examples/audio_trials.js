import 'jspsych';
import 'jspsych/plugins/jspsych-html-keyboard-response'
import snd_440 from './parts/assets/sounds/440.mp3';
//import snd_550 from './parts/assets/sounds/550.mp3';

import get_sound_trials from './parts/present_sounds';

jsPsych.init({
    timeline: [
        {
            type: 'html-keyboard-response',
            stimulus: '<p>Please wait...</p>',
            choices: jsPsych.NO_KEYS,
            trial_duration: 2000
        },
        get_sound_trials(10)
    ],
    preload_audio: [snd_550, snd_440]
});