import 'jspsych'
import 'jspsych/plugins/jspsych-html-keyboard-response'

export default function get_hello() {
    return {
        type: 'html-keyboard-response',
        stimulus: '<p>Thank you for participating</p>' +
            '<p>Please press any key to end this.</p>'
    };
}