import 'jspsych'
import 'jspsych/plugins/jspsych-html-keyboard-response'

export default function get_hello() {
    return {
        type: 'html-keyboard-response',
        stimulus: '<p>Welcome to this experiment.</p>' +
            '<p>This different!</p>' +
            '<p>Please press any key to begin.</p>'
    };
}